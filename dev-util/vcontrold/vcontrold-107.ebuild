# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools

DESCRIPTION="Daemon for communication with Viessmann Vito heatings"
HOMEPAGE="https://github.com/openv/openv/"
SRC_URI="https://dev.gentoo.org/~amynka/snap/${P}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~arm"
IUSE=""

DEPEND="dev-libs/libxml2"
RDEPEND="${DEPEND}"

S="${S}/vcontrold"
PATCHES=("${FILESDIR}/gnusrc.patch")

src_unpack() {
	local S
	S=${WORKDIR}/${P}
	default
}

src_prepare() {
	default
	eautoreconf
}

src_install() {
	emake install DESTDIR="${D}"
	doinitd "${FILESDIR}/vcontrold"
	dodir /etc/vcontrold
	cp -r "${WORKDIR}/${P}/xml-32/"* "${ED}/etc/vcontrold/"
	dodoc ../LIESMICH.txt AUTHORS README
}
