# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{4,5,6})
inherit distutils-r1

DESCRIPTION="Network analysis add-on for Orange data mining suite"
HOMEPAGE="https://github.com/biolab/orange3-network"
SRC_URI="https://github.com/biolab/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-python/networkx-2.0[${PYTHON_USEDEP}]
	>=dev-python/pyqtgraph-0.9.10[${PYTHON_USEDEP}]
	>=dev-python/numpy-1.9.0[${PYTHON_USEDEP}]
	>=dev-util/Orange-3.3.9[${PYTHON_USEDEP}]"

DEPEND="${DEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
        dev-python/setuptools_scm[${PYTHON_USEDEP}]"
