# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit eutils flag-o-matic

DESCRIPTION="PNG23D makes a 3d file from a PNG picture (command line)"
HOMEPAGE="http://kyllikki.github.io/png23d/"
#SRC_URI="https://github.com/kyllikki/png23d/archive/master.zip -> ${P}.zip"
SRC_URI="https://github.com/kyllikki/${PN}/archive/v${PV}.zip"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="media-libs/libpng"
DEPEND="${RDEPEND}
        app-arch/unzip"

#S="/var/tmp/portage/media-gfx/png23d-1.10/work/png23d-master"

#src_unpack() {
#	if [ "${A}" != "" ]; then
#		unpack ${A}
#	fi
#}


src_prepare() {
	filter-flags -Wl,--as-needed
	epatch "${FILESDIR}"/${P}-bitmap.c.patch \
	"${FILESDIR}"/${P}-option.c.patch \
	"${FILESDIR}"/${P}-Makefile.patch
}

src_install() {
	emake DESTDIR="${D}" install
	emake DESTDIR="${D}" install-man
}

#pkg_postrm(){
#	mkdir -p /usr/local/share/man
#}
